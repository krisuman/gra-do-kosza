﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class PauseScreenView : UiView
{
	[Inject]
	private readonly GamePauseSystem _gamePauseSystem;
	
	[SerializeField]
	private Image _overlayImage;
	[SerializeField]
	private RectTransform _contentRect;
	[SerializeField]
	private Button _confirmButton;
	[SerializeField]
	private Button _cancelButton;

	private bool _isDisplayed;
	
	public override void Show()
	{
		if(_isDisplayed) {return;}

		_isDisplayed = true;
		gameObject.SetActive(true);
		_overlayImage.enabled = true;
		_gamePauseSystem.PauseGame();
		_overlayImage.DOFade(0f, 0.5f).From();
		_contentRect.localScale = Vector3.zero;
		_contentRect.DOScale(1f, 0.5f).SetEase(Ease.OutBack);
	}

	private void Start()
	{
		gameObject.SetActive(false);
		_confirmButton.onClick.AddListener(ConfirmClicked);
		_cancelButton.onClick.AddListener(CancelClicked);
	}

	private void ConfirmClicked()
	{
		//TODO: move to system
		UnityEngine.SceneManagement.SceneManager.LoadScene(Constants.OutGameSceneName);
	}

	private void CancelClicked()
	{
		_overlayImage.enabled = false;
		_contentRect.DOScale(0f, 0.3f).OnComplete(() => gameObject.SetActive(false));
		DOVirtual.DelayedCall(0.5f, () =>
		{
			_gamePauseSystem.UnpauseGame();
			_isDisplayed = false;
		});
	}
}
