﻿using LocalizationEditor;
using TMPro;
using UnityEngine;
using Zenject;

public class PlayerScoreView : UiView
{
    [Inject]
    private readonly ScoreChangedSignal _scoreChangedSignal;
    
    [SerializeField]
    private TextMeshProUGUI _scoreText;
    [SerializeField]
    private TextMeshProUGUI _playerNameText;
    
    
    private Player _player;
    
    public void Init(Player player)
    {
        _player = player;

        if (null == player)
        {
            gameObject.SetActive(false);
            return;
        }

        _playerNameText.text = string.Format("{0} {1}", LEManager.Get(LEKeys.player), _player.Id + 1);
        
        SetScore();
        _scoreChangedSignal.Listen(SetScore);
    }

    private void SetScore()
    {
        _scoreText.text = _player.Score.ToString();
    }
}
