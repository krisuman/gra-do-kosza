﻿using UnityEngine;
using Zenject;

public class ScoresView : UiView
{
    [SerializeField]
    private Transform[] _playerScoreHolders;
    [SerializeField]
    private PlayerScoreView _playerScoreViewPrefab;

    [Inject]
    private readonly GamePlayersHolderSystem _playersHolder;
    
    private void Start()
    {
        for (var i = 0; i < Constants.MaxNumberOfPlayers; i++)
        {
            var newPlayerScore = CreateNewView(_playerScoreViewPrefab);
            newPlayerScore.transform.SetParent(_playerScoreHolders[i]);
            newPlayerScore.transform.localPosition = Vector3.zero;
            newPlayerScore.transform.localRotation = Quaternion.identity;
            newPlayerScore.Init(_playersHolder.Players.Count > i ? _playersHolder.Players[i] : null);
        }
    }
}
