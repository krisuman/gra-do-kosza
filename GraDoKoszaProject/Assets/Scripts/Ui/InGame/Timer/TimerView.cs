﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class TimerView : UiView
{
    [Inject]
    private TimeChangedSignal _timeChangedSignal;
    [Inject]
    private GameGeneralSettings _gameGeneralSettings;

    [SerializeField]
    private RectTransform _fillHolderRect;
    [SerializeField]
    private RectTransform _fillImageRect;
    [SerializeField]
    private TextMeshProUGUI _timerText;
    [SerializeField]
    private Color _regularTextColor;
    [SerializeField]
    private Color _lowTimeTextColor;

    private int _startTime;
    
    private void Start()
    {
        _timeChangedSignal.Listen(TimeChanged);
        _startTime = _gameGeneralSettings.GameplayTime;
        TimeChanged(_startTime);
    }

    private void TimeChanged(int time)
    {
        SetTimerText(time);
        SetTimerFillImage(time);
    }

    private void SetTimerText(int time)
    {
        var t = TimeSpan.FromSeconds(time);
        var timerDisplay = string.Format("{0:D2}:{1:D2}", 
            t.Minutes, 
            t.Seconds);

        _timerText.text = timerDisplay;
    }

    private void SetTimerFillImage(int time)
    {
        const float startValue = -15f;
        var endValue = _fillHolderRect.rect.size.x;

        var timeLerp = Mathf.InverseLerp(0, _startTime, time);
        var offsetValue = Mathf.Lerp(endValue, startValue, timeLerp);

        DOTween.To(() =>_fillImageRect.offsetMax,
            x => _fillImageRect.offsetMax = x,  new Vector2(-offsetValue, _fillImageRect.offsetMax.y), 0.5f);

        var lowTime = time > _gameGeneralSettings.GameplayLowTime;     
        _timerText.color = lowTime ? _regularTextColor : _lowTimeTextColor;
    }
}
