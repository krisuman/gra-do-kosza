﻿using DG.Tweening;
using UnityEngine;
using Zenject;

public class DisplayesView : UiView
{
    [Inject]
    private readonly GameOverSignal _gameOverSignal;
    
    [SerializeField]
    private RectTransform _leftDisplayesRect;
    [SerializeField]
    private RectTransform _rightDisplayesRect;

    private void Start()
    {
        _gameOverSignal.Listen(HideDispalyes);
    }

    private void HideDispalyes()
    {
        _leftDisplayesRect.DOAnchorPos(new Vector2(-500, 0), 2f);
        _rightDisplayesRect.DOAnchorPos(new Vector2(500, 0), 2f);
    }
}
