﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ResultsView : UiView
{
	private const float StartingOffset = 730f;
	
	[SerializeField]
	private RectTransform _resultsEntriesHolder;
	[SerializeField]
	private ResultEntryView _resultEntryPrefab;

	private readonly List<ResultEntryView> _resultEntries= new List<ResultEntryView>();
	
	public void ShowResults(List<Player> players)
	{
		for (var i = 0; i < _resultEntries.Count; i++)
		{
			_resultEntries[i].Init(players.Count > i ? players[i] : null);
		}

		_resultsEntriesHolder.DOAnchorPos(Vector2.zero, players.Count /2f);
	}

	private void Awake()
	{
		for (var i = 0; i < Constants.MaxNumberOfPlayers; i++)
		{
			var newEntry = CreateNewView(_resultEntryPrefab);
			newEntry.transform.SetParent(_resultsEntriesHolder);
			newEntry.transform.localScale = Vector3.one;
			newEntry.transform.localPosition = Vector3.zero;
			_resultEntries.Add(newEntry);
		}
		
		_resultsEntriesHolder.anchoredPosition = new Vector2(0, StartingOffset);
	}
}
