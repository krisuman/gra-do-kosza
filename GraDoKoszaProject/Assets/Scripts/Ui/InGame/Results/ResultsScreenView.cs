﻿using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ResultsScreenView : UiView
{
    [Inject]
    private GameOverSignal _gameOverSignal;
    [Inject]
    private GamePlayersHolderSystem _playersHolder;
    
    [SerializeField]
    private Image _overlayImage;
    [SerializeField]
    private RectTransform _contentRect;
    [SerializeField]
    private ResultsView[] _resultsViews;
    [SerializeField]
    private GameObject _sorterObject;

    private void Start()
    {
        gameObject.SetActive(false);
        _gameOverSignal.Listen(ShowScreen);
    }

    private void ShowScreen()
    {
        gameObject.SetActive(true);
        _overlayImage.DOFade(0, 4f).From();

        _sorterObject.SetActive(true);
        
        _contentRect.localScale = Vector3.zero;
        _contentRect.DOScale(1, 1f).SetEase(Ease.OutBack).SetDelay(2f).OnComplete(ShowResults);
    }

    private void ShowResults()
    {
        foreach (var resultsView in _resultsViews)
        {
            resultsView.ShowResults(_playersHolder.Players.Values.OrderBy(x => x.ResultPosition).ToList());
        }
    }
}
