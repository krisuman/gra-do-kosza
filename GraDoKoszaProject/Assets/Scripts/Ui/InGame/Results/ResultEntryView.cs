﻿using LocalizationEditor;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ResultEntryView : UiView
{
    [Inject]
    private readonly GameGeneralSettings _gameGeneralSettings;
    
    [SerializeField]
    private TextMeshProUGUI _playerNameText;
    [SerializeField]
    private TextMeshProUGUI _playerResultText;
    [SerializeField]
    private Image _medalImage;
    [SerializeField]
    private Image _backgroundImage;
    [SerializeField]
    private Image _flashImage;
    [SerializeField]
    private Color[] _backgroundColors;

    public void Init(Player player)
    {
        if (null == player)
        {
            gameObject.SetActive(false);
            return;
        }

        _playerNameText.text = string.Format("{0} {1}", LEManager.Get(LEKeys.player), player.Id + 1);
        _playerResultText.text = player.Score.ToString();
        
        _backgroundImage.color = _backgroundColors[player.ResultPosition - 1];

        if (player.ResultPosition <= _gameGeneralSettings.MedalSprites.Length)
        {
            _medalImage.sprite = _gameGeneralSettings.MedalSprites[player.ResultPosition - 1];
        }
        else
        {
            _flashImage.gameObject.SetActive(false);
            _medalImage.gameObject.SetActive(false);
        }
    }
}
