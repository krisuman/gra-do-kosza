﻿using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;
using Zenject;

public class EndGameView : UiView
{
    [Inject]
    private readonly GameOverSignal _gameOverSignal;
    
    [SerializeField]
    private Button _endButton;
    [SerializeField]
    private PauseScreenView _pauseScreen;

    private void Start()
    {
        _endButton.onClick.AddListener(PauseGame);
        _gameOverSignal.Listen(ChangeEvents);
    }

    private void ChangeEvents()
    {
        _endButton.onClick.RemoveAllListeners();
        _endButton.onClick.AddListener(QuitGame);
    }
    
    private void PauseGame()
    {
        _pauseScreen.Show();
    }

    private void QuitGame()
    {
        //TODO: move to system
        UnityEngine.SceneManagement.SceneManager.LoadScene(Constants.OutGameSceneName);
    }
}
