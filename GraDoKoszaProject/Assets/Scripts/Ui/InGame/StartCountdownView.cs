﻿using System.Collections;
using TMPro;
using UnityEngine;

public class StartCountdownView : UiView
{
    [SerializeField]
    private Animator _sorterAnimator;

    private IEnumerator Start()
    {
        yield return null;
        _sorterAnimator.Play("Sorter");
    }
}
