﻿using DG.Tweening;
using UnityEngine;

[System.Serializable]
public class PlayerStearingView
{
    [SerializeField]
    private HoldableButton _turnLefButton;
    [SerializeField]
    private HoldableButton _turnRightButton;
    
    private Player _player;
    
    public void Init(Player player)
    {
        _player = player;
        
        _turnLefButton.onHold.AddListener(_player.TurnLeft);
        _turnRightButton.onHold.AddListener(_player.TurnRight);
    }

    public void Hide()
    {
        _turnLefButton.transform.DOScale(0, 0.5f);
        _turnRightButton.transform.DOScale(0, 0.5f);
    }
}
