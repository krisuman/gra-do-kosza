﻿using DG.Tweening;
using LocalizationEditor;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class PlayerControllerView : UiView
{
    [Inject]
    private readonly GameBinsSettings _binsSettings;
    [Inject]
    private readonly BinsChangedSignal _binsChangedSignal;
    [Inject]
    private readonly GameOverSignal _gameOverSignal;
    [Inject]
    private readonly GameGeneralSettings _gameGeneralSettings;
    
    [SerializeField]
    private PlayerStearingView _stearingView;
    [SerializeField]
    private TextMeshProUGUI _playerNameText;
    [SerializeField]
    private Image _medalImage;
    [SerializeField]
    private Image _binIcon;
    
    private Player _player;
    private PlayerControllerHolderView _holderView;
    
    public void Init(Player player, PlayerControllerHolderView holderView)
    {
        _medalImage.gameObject.SetActive(false);
        _player = player;
        _holderView = holderView;
        
        if (null == _player)
        {
            _holderView.gameObject.SetActive(false);
            return;
        }
        
        _stearingView.Init(_player);
        //TODO: move to some helper and replace in all places
        _playerNameText.text = string.Format("{0} {1}", LEManager.Get(LEKeys.player), _player.Id + 1);

        ChangeColor();
        _binsChangedSignal.Listen(ChangeColor);
        
        _gameOverSignal.Listen(SwitchToResultsView);
    }

    private void ChangeColor()
    {
        _holderView.BackgorundImage.DOColor(_binsSettings.Bins[_player.BinType].BinColor, 0.3f);
        _binIcon.sprite = _binsSettings.Bins[_player.BinType].BinLogo;
    }

    private void SwitchToResultsView()
    {
        if (_player.ResultPosition <= 3)
        {
            _medalImage.gameObject.SetActive(true);
            _medalImage.sprite = _gameGeneralSettings.MedalSprites[_player.ResultPosition - 1];
            _medalImage.transform.DOScale(0, 0.5f).SetEase(Ease.OutBack).From().SetDelay(4);
        }

        _binIcon.gameObject.SetActive(false);
        (_playerNameText.transform as RectTransform).DOAnchorPosY(0, 0.5f);
        _stearingView.Hide();
    }
}
