﻿using UnityEngine;
using Zenject;

public class PlayerControllersView : UiView
{
    [SerializeField]
    private PlayerControllerHolderView[] _playerControllerHolders;
    [SerializeField]
    private PlayerControllerView _playerControllerViewPrefab;

    [Inject]
    private GamePlayersHolderSystem _playersHolder;

    private void Start()
    {
        for (var i = 0; i < Constants.MaxNumberOfPlayers; i++)
        {
            var newController = CreateNewView(_playerControllerViewPrefab);
            newController.transform.SetParent(_playerControllerHolders[i].transform);
            newController.transform.localPosition = Vector3.zero;
            newController.transform.localRotation = Quaternion.identity;
            newController.Init(_playersHolder.Players.Count > i ? _playersHolder.Players[i] : null, _playerControllerHolders[i]);
        }
    }
}
