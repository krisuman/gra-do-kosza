﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerControllerHolderView : UiView
{
    [SerializeField]
    private Image _holderBackgroundImage;
    
    public Image BackgorundImage
    {
        get { return _holderBackgroundImage; }
    }
}
