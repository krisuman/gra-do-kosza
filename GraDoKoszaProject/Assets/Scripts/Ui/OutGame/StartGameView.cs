﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class StartGameView : UiView
{
    [SerializeField]
    private Button _startGameButton;
    
    [Inject]
    private StartIngameSystem _startIngameSystem;

    private void Start()
    {
        _startGameButton.onClick.AddListener(_startIngameSystem.StartIngame);
    }
}
