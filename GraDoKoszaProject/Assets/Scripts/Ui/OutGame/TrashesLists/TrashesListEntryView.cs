﻿using LocalizationEditor;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TrashesListEntryView : UiView
{
    [SerializeField]
    private Image _iconImage;
    [SerializeField]
    private TextMeshProUGUI _nameText;

    private TrashData _trash;
    
    public void Init(TrashData trash)
    {
        _trash = trash;
        _iconImage.sprite = trash.TrashIcon;
        SetName();
        LEManager.OnNewLanguageSelected += SetName;
    }

    private void SetName()
    {
        _nameText.text = LEManager.Get(_trash.LocalizationId);
    }
}
