﻿using UnityEngine;
using UnityEngine.UI;

public class TrashesDoubleListView : UiView
{
    [SerializeField]
    private Image _backgroundImage;
    [SerializeField]
    private Outline _backgroundOutline;
    [SerializeField]
    private Image[] _lineImages;
    [SerializeField]
    private Image[] _binIcons;
    [SerializeField]
    private TrashesListView[] _listViews;

    private Bin _bin;
    
    public void Init(Bin bin)
    {
        _bin = bin;
        
        _backgroundImage.color = bin.BinColor;
        _backgroundOutline.effectColor = bin.BinSecondaryColor;
        
        foreach (var line in _lineImages)
        {
            line.color = bin.BinSecondaryColor;
        }

        foreach (var listView in _listViews)
        {
            listView.Init(bin.BinType);
        }
    }

    public override void Show()
    {
        foreach (var icon in _binIcons)
        {
            icon.sprite = _bin.BinLogo;
        }
    }
}
