﻿using UnityEngine;
using Zenject;

public class TrashesListView : UiView
{
	[Inject]
	private readonly GameTrashesSettings _trashesSettings;

	[SerializeField]
	private Transform _entriesHolder;
	[SerializeField]
	private TrashesListEntryView _trashesListEntryPrefab;

	public void Init(BinType binType)
	{
		var myTrashes = _trashesSettings.Trashes[binType];
		foreach (var trash in myTrashes)
		{
			var newTrash = CreateNewView(_trashesListEntryPrefab);
			newTrash.transform.SetParent(_entriesHolder);
			newTrash.Init(trash);
		}
	}
}
