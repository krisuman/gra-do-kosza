﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class TrashesListsView : UiView
{  
    [Inject]
    private readonly GameBinsSettings _binsSettings;

    [SerializeField]
    private Image _overaImage;
    [SerializeField]
    private Button _closeButton;
    [SerializeField]
    private Transform _listsHolder;
    [SerializeField]
    private TrashesDoubleListView _trashesDoubleListPrefab;
    
    private List<TrashesDoubleListView> _trashesDoubleListViewList = new List<TrashesDoubleListView>();

    public override void Show()
    {
        gameObject.SetActive(true);
        _overaImage.color = new Color(0,0,0,0);
        _overaImage.DOFade(0.7f, 0.5f);
        _listsHolder.localScale = new Vector3(1f, 0f, 1f);
        _listsHolder.DOScaleY(1f, 0.3f).SetEase(Ease.OutBack);
        _closeButton.transform.localScale = Vector3.zero;
        _closeButton.transform.DOScale(1f, 0.2f);

        foreach (var doubleList in _trashesDoubleListViewList)
        {
            doubleList.Show(); 
        }
    }

    public override void Hide()
    {
        _closeButton.transform.DOScale(0f, 0.2f);
        _overaImage.DOFade(0f, 0.3f).OnComplete(() => gameObject.SetActive(false));
        _listsHolder.DOScaleY(0f, 0.2f);
    }
    
    private void Start()
    {
        _closeButton.onClick.AddListener(Hide);
        
        foreach (var bin in _binsSettings.Bins)
        {
            var newDoubleList = CreateNewView(_trashesDoubleListPrefab);
            newDoubleList.transform.SetParent(_listsHolder);
            newDoubleList.Init(bin.Value);
            _trashesDoubleListViewList.Add(newDoubleList);
        }
        
        gameObject.SetActive(false);
    }
}
