﻿using DG.Tweening;
using LocalizationEditor;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

//TODO: move logic from here to seperated system
public class DifficultySelectionView : UiView
{
    [Inject]
    private ProjectGeneralSettings _projectGeneralSettings;
    
    [SerializeField]
    private Button _rightButton;
    [SerializeField]
    private Button _leftButton;
    [SerializeField]
    private TextMeshProUGUI _difficultyText;
    [SerializeField]
    private Color _easyColor;
    [SerializeField]
    private Color _hardColor;
    [SerializeField]
    private Color _veryHardColor;

    private Tweener _textTweener;
    
    private void Start()
    {
        _rightButton.onClick.AddListener(ChangeDifficultyUp);
        _leftButton.onClick.AddListener(ChangeDifficultyDown);

        LEManager.OnNewLanguageSelected += SetDifficultyName;
        SetDifficultyName();
    }

    private void ChangeDifficultyUp()
    {
        var current = _projectGeneralSettings.DifficultyLevel;
        var next = DifficultyLevel.Easy;
        
        switch (current)
        {
            case DifficultyLevel.Easy:
                next = DifficultyLevel.Hard;
                break;
            case DifficultyLevel.Hard:
                next = DifficultyLevel.VeryHard;
                break;
            case DifficultyLevel.VeryHard:
                next = DifficultyLevel.Easy;
                break;
        }

        _projectGeneralSettings.DifficultyLevel = next;
        
        AnimateDifficulty();
    }

    private void ChangeDifficultyDown()
    {
        var current = _projectGeneralSettings.DifficultyLevel;
        var next = DifficultyLevel.Easy;
        
        switch (current)
        {
            case DifficultyLevel.Easy:
                next = DifficultyLevel.VeryHard;
                break;
            case DifficultyLevel.Hard:
                next = DifficultyLevel.Easy;
                break;
            case DifficultyLevel.VeryHard:
                next = DifficultyLevel.Hard;
                break;
        }

        _projectGeneralSettings.DifficultyLevel = next;
        
        AnimateDifficulty();
    }

    private void AnimateDifficulty()
    {
        _textTweener.Kill();
        _difficultyText.transform.localScale = new Vector3(0,1,1);
        _textTweener = _difficultyText.transform.DOScale(1f, 0.3f);
        
        SetDifficultyName();
    }
    
    private void SetDifficultyName()
    {
        var isEasy = _projectGeneralSettings.DifficultyLevel == DifficultyLevel.Easy;

        var textToSet = "";
        var color = Color.white;
        switch (_projectGeneralSettings.DifficultyLevel)
        {
            case DifficultyLevel.Easy:
                textToSet = LEManager.Get(LEKeys.difficulty_easy);
                color = _easyColor;
                break;
            case DifficultyLevel.Hard:
                textToSet = LEManager.Get(LEKeys.difficulty_hard);
                color = _hardColor;
                break;
            case DifficultyLevel.VeryHard:
                textToSet = LEManager.Get(LEKeys.difficulty_very_hard);
                color = _veryHardColor;
                break;
        }

        _difficultyText.text = textToSet;
        _difficultyText.color = color;
    }
}
