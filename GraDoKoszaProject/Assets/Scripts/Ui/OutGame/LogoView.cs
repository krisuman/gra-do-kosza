﻿using LocalizationEditor;
using UnityEngine;

public class LogoView : UiView
{
    [SerializeField]
    private GameObject _polishLogo;
    [SerializeField]
    private GameObject _englishLogo;

    private void Start()
    {
        LEManager.OnNewLanguageSelected += SetLogo;
        SetLogo();
    }

    private void OnDestroy()
    {
        LEManager.OnNewLanguageSelected -= SetLogo;
    }

    private void SetLogo()
    {
        var isEnglish = LEManager.CurrentLocSet == Constants.EnglishLanguageSet;
        _polishLogo.SetActive(!isEnglish);
        _englishLogo.SetActive(isEnglish);
    }
}
