﻿using UnityEngine;

public class SuperSorterHomeView : UiView
{
    [SerializeField]
    private Animator _animator;

    private readonly int _helloTrigger = Animator.StringToHash("hello");

    private float _lastTime;
    private float _waitTime;
    
    private void Update()
    {
        if (Time.realtimeSinceStartup - _lastTime > _waitTime)
        {
            _animator.SetTrigger(_helloTrigger);
            _lastTime = Time.realtimeSinceStartup;
            _waitTime = Random.Range(5f, 15f);
        }
    }
}
