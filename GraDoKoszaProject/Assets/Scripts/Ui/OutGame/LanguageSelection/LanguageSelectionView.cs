﻿using LocalizationEditor;
using UnityEngine;
using UnityEngine.UI;

//TODO: someday move logic from this class to some system
public class LanguageSelectionView : UiView
{
    [SerializeField]
    private Button _polishLanguageButton;
    [SerializeField]
    private Button _englishLanguageButton;
    [SerializeField]
    private Image _polishFlagImage;
    [SerializeField]
    private Image _englishFlagImage;
    [SerializeField]
    private Color _activeLanguageColor;
    [SerializeField]
    private Color _disabledLanguageColor;

    private void Start()
    {
        _polishLanguageButton.onClick.AddListener(SetPolishLanguage);
        _englishLanguageButton.onClick.AddListener(SetEnglishLanguage);
        
        SetPolishLanguage();
    }

    private void SetPolishLanguage()
    {
        LEManager.CurrentLocSet = Constants.PolishLanguageSet;
        _polishFlagImage.color = _activeLanguageColor;
        _englishFlagImage.color = _disabledLanguageColor;
    }

    private void SetEnglishLanguage()
    {
        LEManager.CurrentLocSet = Constants.EnglishLanguageSet;
        _polishFlagImage.color = _disabledLanguageColor;
        _englishFlagImage.color = _activeLanguageColor;
    }
}
