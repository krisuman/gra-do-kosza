﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ChooseNumberOfPlayersView : UiView
{
    [Inject]
    private PlayersSelectionSystem _playersSelectionSystem;
    [Inject]
    private ProjectGeneralSettings _generalGameSettings;
    [Inject]
    private NumberOfPlayersChangedSignal _numberOfPlayersChangedSignal;
    
    [SerializeField]
    private Button _leftButton;
    [SerializeField]
    private Button _rightButton;
    [SerializeField]
    private TextMeshProUGUI _numberOfPlayersText;

    private Tweener _numberTweener;

    private void Start()
    {
        _leftButton.onClick.AddListener(_playersSelectionSystem.DecriseNumberOfPlayers);
        _rightButton.onClick.AddListener(_playersSelectionSystem.IncreaseNumberOfPlayers);
        
        _numberOfPlayersChangedSignal.Listen(NumberOfPlayersChanged);
        NumberOfPlayersChanged(_generalGameSettings.NumberOfPlayers);
    }

    private void NumberOfPlayersChanged(int numberOfPlayers)
    {
        _numberTweener.Kill();
        _numberOfPlayersText.transform.localScale = Vector3.zero;
        _numberTweener = _numberOfPlayersText.transform.DOScale(1f, 0.3f).SetEase(Ease.OutBack);
        _numberOfPlayersText.text = numberOfPlayers.ToString();

        _leftButton.interactable = numberOfPlayers > Constants.MinNumberOfPlayers;
        _rightButton.interactable = numberOfPlayers < Constants.MaxNumberOfPlayers;
    }
}
