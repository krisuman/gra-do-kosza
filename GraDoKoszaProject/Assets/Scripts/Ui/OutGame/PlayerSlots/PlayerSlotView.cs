﻿using DG.Tweening;
using LocalizationEditor;
using TMPro;
using UnityEngine;
using Zenject;

public class PlayerSlotView : UiView
{    
    [Inject]
    private readonly NumberOfPlayersChangedSignal _numberOfPlayersChangedSignal;
    [Inject]
    private readonly ProjectGeneralSettings _projectGeneralSettings;
    
    [SerializeField]
    private TextMeshProUGUI _playerNameText;
    [SerializeField]
    private Color _enabledPlayerColor;
    [SerializeField]
    private Color _disabledPlayerColor;

    private int _playerNumber;
    private Tweener _slotBackgroundTweener;
    private PlayerSlotHolderView _slotHolder;

    public void Init(int playerNumber, PlayerSlotHolderView slotHolder)
    {
        _playerNumber = playerNumber;
        _slotHolder = slotHolder;
        
        SetPlayerName();
        LEManager.OnNewLanguageSelected += SetPlayerName;
        
        _playerNameText.color = _enabledPlayerColor;
        _numberOfPlayersChangedSignal.Listen(UpdateView);
        
        UpdateView(_projectGeneralSettings.NumberOfPlayers);
    }

    private void SetPlayerName()
    {
        _playerNameText.text = string.Format("{0} {1}", LEManager.Get(LEKeys.player), _playerNumber);
    }
    
    private void UpdateView(int number)
    {
        var active = number >= _playerNumber;
        _playerNameText.color = active ? _enabledPlayerColor : _disabledPlayerColor;
        AnimateBackground(active);
    }

    private void AnimateBackground(bool enable)
    {
        _slotBackgroundTweener.Kill();
        _slotBackgroundTweener = _slotHolder.BackgorundImage.DOFade(enable ? 1f : 0f, 0.5f);
    }
}
