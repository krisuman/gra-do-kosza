﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerSlotHolderView : UiView 
{
    [SerializeField]
    private Image _holderBackgroundImage;
    
    public Image BackgorundImage
    {
        get { return _holderBackgroundImage; }
    }
}
