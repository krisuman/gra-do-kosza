﻿using UnityEngine;

public class PlayerSlotsView : UiView
{
    [SerializeField]
    private PlayerSlotHolderView[] _playerSlotsHolders;
    [SerializeField]
    private PlayerSlotView _playerSlotViewPrefab;

    private void Start()
    {
        for (var i = 0; i < Constants.MaxNumberOfPlayers; i++)
        {
            var newController = CreateNewView(_playerSlotViewPrefab);
            newController.transform.SetParent(_playerSlotsHolders[i].transform);
            newController.transform.localPosition = Vector3.zero;
            newController.transform.localRotation = Quaternion.identity;
            newController.Init(i + 1, _playerSlotsHolders[i]);
        }
    }
}
