﻿using ModestTree;
using UnityEngine;
using Zenject;

/// <inheritdoc />
/// <summary>
/// Base ui view class. 
/// Provides easy way to create new neasted views.
/// </summary>
public abstract class UiView : MonoBehaviour
{
    [Inject]
    private DiContainer _container;

    /// <summary>
    /// Creates new instance of provided View and parents it to this view
    /// </summary>
    /// <typeparam name="T">View</typeparam>
    /// <param name="view">View to create</param>
    /// <returns></returns>
    protected T CreateNewView<T>(T view) where T : UiView
    {
        return view != null ? _container.InstantiatePrefab(view, transform).GetComponent<T>() : null; 
    }

    /// <summary>
    /// Validates reference
    /// </summary>
    protected void Validate(Object o, string name)
    {
        Assert.IsNotNull(o, string.Format("[UI] {0}: {1} missing reference!", this.name, name));
    }

    /// <summary>
    /// Displays view
    /// </summary>
    public virtual void Show()
    {
        gameObject.SetActive(true);
    }

    /// <summary>
    /// Hides view
    /// </summary>
    public virtual void Hide()
    {
        gameObject.SetActive(false);
    }
}

