﻿using Zenject;

public class OutGameInstaller : MonoInstaller 
{
    public override void InstallBindings()
    {
        //Systems
        Container.BindInterfacesAndSelfTo<StartIngameSystem>().AsSingle();
        Container.BindInterfacesAndSelfTo<PlayersSelectionSystem>().AsSingle();
        
        //Signals
        Container.DeclareSignal<NumberOfPlayersChangedSignal>();
    }
}
