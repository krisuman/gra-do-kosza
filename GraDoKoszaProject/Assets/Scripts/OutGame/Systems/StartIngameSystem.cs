﻿public class StartIngameSystem
{
    private readonly SceneTransitionSystem _sceneTransitionSystem;

    public StartIngameSystem(SceneTransitionSystem sceneTransitionSystem)
    {
        _sceneTransitionSystem = sceneTransitionSystem;
    }
    
    public void StartIngame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(Constants.InGameSceneName);

        //TODO: find why this produce nulls after switching
        //_sceneTransitionSystem.LoadScene(Constants.InGameSceneName);
    }
}
