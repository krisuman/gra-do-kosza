﻿public class PlayersSelectionSystem
{
    private const int InitialNumberOfPlayers = 2;
    
    private readonly ProjectGeneralSettings _generalGameSettings;
    private readonly NumberOfPlayersChangedSignal _numberOfPlayersChangedSignal;

    private int _numberOfPlayers;
    
    private int NumberOfPlayers
    {
        get { return _numberOfPlayers; }
        set
        {
            _numberOfPlayers = value;
            _generalGameSettings.NumberOfPlayers = value;
        }
    }
    
    public PlayersSelectionSystem(
        NumberOfPlayersChangedSignal numberOfPlayersChangedSignal,
        ProjectGeneralSettings generalGameSettings)
    {
        _numberOfPlayersChangedSignal = numberOfPlayersChangedSignal;
        _generalGameSettings = generalGameSettings;

        NumberOfPlayers = InitialNumberOfPlayers;
    }

    public void IncreaseNumberOfPlayers()
    {
        if (_numberOfPlayers == Constants.MaxNumberOfPlayers)
        {
            return;
        }

        NumberOfPlayers++;
        _numberOfPlayersChangedSignal.Fire(NumberOfPlayers);
    }

    public void DecriseNumberOfPlayers()
    {
        if (_numberOfPlayers == Constants.MinNumberOfPlayers)
        {
            return;
        }

        NumberOfPlayers--;
        _numberOfPlayersChangedSignal.Fire(NumberOfPlayers);
    }
}
