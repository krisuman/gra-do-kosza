﻿using UnityEngine;
using Zenject;

[CreateAssetMenu(menuName = "GraDoKosza/ProjectGeneralSettings")]
public class ProjectGeneralSettings : ScriptableObjectInstaller
{
    [SerializeField]
    private int _numberOfPlayers = 2;
    [SerializeField]
    private DifficultyLevel _difficultyLevel = DifficultyLevel.Hard;
    
    public int NumberOfPlayers
    {
        get { return _numberOfPlayers; }
        set { _numberOfPlayers = value; }
    }
    
    public DifficultyLevel DifficultyLevel
    {
        get { return _difficultyLevel; }
        set { _difficultyLevel = value; }
    }
    
    public override void InstallBindings()
    {
        Container.BindInstance(this).AsSingle();
    }
}

public enum DifficultyLevel
{
    Easy = 0,
    Hard = 1,
    VeryHard = 2
}
