﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransitionSystem
{    
    private readonly AsyncProcessor _asyncProcessor;
    
    private string currentSceneName;
    private string sceneToLoad;

    public SceneTransitionSystem(AsyncProcessor asyncProcessor)
    {
        _asyncProcessor = asyncProcessor;
    }

    public void LoadSceneAfterDelay(string sceneName, float delay)
    {
        currentSceneName = SceneManager.GetActiveScene().name;
        _asyncProcessor.StartCoroutine(ReloadSceneAfterDelayCoroutine(sceneName, delay));
    }

    public void LoadScene(string sceneName)
    {
        currentSceneName = SceneManager.GetActiveScene().name;
        sceneToLoad = sceneName;

        SceneManager.LoadSceneAsync(Constants.TransitionSceneName, LoadSceneMode.Additive);
        SceneManager.sceneLoaded += OnLoadSceneLoaded;
    }
    
    private IEnumerator ReloadSceneAfterDelayCoroutine(string sceneName, float delay)
    {
        yield return new WaitForSeconds(delay);
        LoadScene(sceneName);
    }

    private void OnLoadSceneLoaded(Scene scene, LoadSceneMode loadMode)
    {
        SceneManager.sceneLoaded -= OnLoadSceneLoaded;
        SceneManager.UnloadSceneAsync(currentSceneName);

        //handle animations here when have assets

        SceneManager.sceneUnloaded += OnCurrentSceneUnloaded;
    }

    private void OnCurrentSceneUnloaded(Scene scene)
    {
        SceneManager.sceneUnloaded -= OnCurrentSceneUnloaded;

        SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Additive);
        SceneManager.sceneLoaded += OnWantedSceneLoaded;
    }

    private void OnWantedSceneLoaded(Scene scene, LoadSceneMode loadMode)
    {
       // SceneManager.SetActiveScene(scene);
        SceneManager.sceneLoaded -= OnWantedSceneLoaded;
        SceneManager.UnloadSceneAsync(Constants.TransitionSceneName);
        //currentSceneName = sceneToLoad;
    }
}

