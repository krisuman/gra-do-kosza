﻿using UnityEngine;
using Zenject;

public class ProjectInstaller : MonoInstaller 
{
    public override void InstallBindings()
    {
        Application.targetFrameRate = 30;
        
        Container.BindInterfacesAndSelfTo<SceneTransitionSystem>().AsSingle();
    }
}
