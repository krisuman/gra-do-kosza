﻿public static class Constants
{
    public const string OutGameSceneName = "main";
    public const string InGameSceneName = "game";
    public const string TransitionSceneName = "transition";
    
    public const int MaxNumberOfPlayers = 4;
    public const int MinNumberOfPlayers = 1;

    public const string PolishLanguageSet = "pl-PL";
    public const string EnglishLanguageSet = "en-US";
}
