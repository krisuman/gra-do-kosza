﻿using DG.Tweening;
using UnityEngine;
using Zenject;

public class PointsEffectView : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer _pointRenderer;
    [SerializeField]
    private Sprite _plusSprite;
    [SerializeField]
    private Sprite _minusSprite;

    private void PlayEffect(bool plusPoints)
    {
        _pointRenderer.transform.localScale = Vector3.zero;
        _pointRenderer.sprite = plusPoints ? _plusSprite : _minusSprite;
        _pointRenderer.color = Color.white;
        _pointRenderer.DOFade(0, 1f).SetDelay(0.3f);
        _pointRenderer.transform.DOScale(1.5f, 0.5f).SetEase(Ease.OutBack);
    }
    
    public class Pool : MemoryPool<Vector2, bool, PointsEffectView>
    {
        protected override void OnCreated(PointsEffectView item)
        {
            base.OnCreated(item);
            item.gameObject.SetActive(false);
        }

        protected override void Reinitialize(Vector2 positon, bool plusPoints, PointsEffectView item)
        {
            item.gameObject.SetActive(true);
            item.transform.position = positon;
            item.PlayEffect(plusPoints);
        }
    }
}