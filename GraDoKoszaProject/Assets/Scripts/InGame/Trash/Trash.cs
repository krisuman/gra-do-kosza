﻿using DG.Tweening;
using UnityEngine;
using Zenject;

public class Trash
{
    private TrashView _trashView;

    public BinType BinType { get; private set; }

    public string Name { get; private set; }
    
    public Trash(GameGeneralSettings generalSettings)
    {
        _trashView = Object.Instantiate(generalSettings.TrashView);
        _trashView.Disable();
    }

    public class Pool : MemoryPool<BinType, TrashData, Vector2, Trash>
    {
        protected override void Reinitialize(BinType binType, TrashData trashData, Vector2 positon, Trash item)
        {
            base.Reinitialize(binType, trashData, positon, item);
            item.Name = trashData.Name;
            item._trashView = Object.Instantiate(trashData.TrashPrefab);
            item._trashView.Enable(item);
            item._trashView.transform.rotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(1, 3) * 180));
            //item._trashView.TrashSpriteRenderer.color = new Color(1, 1, 1, 0);
            //item._trashView.TrashSpriteRenderer.DOFade(1f, 0.3f).SetEase(Ease.OutBack).SetDelay(0.5f);
            item.BinType = binType;
            item._trashView.Position = positon;
        }

        protected override void OnDespawned(Trash item)
        {
            base.OnDespawned(item);
            Object.Destroy(item._trashView.gameObject);
        }
    }
}
