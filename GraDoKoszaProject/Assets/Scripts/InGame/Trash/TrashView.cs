﻿using UnityEngine;

public class TrashView : GameElementVIew
{
    [SerializeField]
    private SpriteRenderer _trashSpriteRenderer;
    [SerializeField]
    private Collider2D _collider;
    
    public Trash ParentLink { get; private set; }
    
    public Collider2D Collider
    {
        get { return _collider; }
    }

    public Vector2 Position
    {
        get { return transform.position; }
        set { transform.position = value; }
    }
    
    public SpriteRenderer TrashSpriteRenderer
    {
        get { return _trashSpriteRenderer; }
    }

    public void Enable(Trash parent)
    {
        gameObject.SetActive(true);
        ParentLink = parent;
    }

    public void Disable()
    {
        gameObject.SetActive(false);
    }
}
