﻿using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller
{
    [Inject]
    private readonly ProjectGeneralSettings _gameSettings;
    [Inject]
    private readonly GameGeneralSettings _gameGeneralSettings;

    [SerializeField]
    private GameDrivingSettings _easyDrivingSettings;
    [SerializeField]
    private GameDrivingSettings _hardDrivingSettings;
    [SerializeField]
    private GameDrivingSettings _veryHardDrivingSettings;
    
    public override void InstallBindings()
    {
        //Systems
        Container.BindInterfacesAndSelfTo<GameStartSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<GameBorderSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<GamePlayersHolderSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<GameViewsHolderSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<GameSetupSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<GameBinsSwitchSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<GameTrashCreatingSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<GameTrashCollectSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<GameTrashDestroySystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<GameScoreSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<GameTimerSystem>().AsSingle().NonLazy();    
        Container.BindInterfacesAndSelfTo<GamePauseSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<GameOverSystem>().AsSingle().NonLazy();
           
        //CHEATS
        Container.BindInterfacesAndSelfTo<TestPlayerController>().AsSingle().NonLazy();
        
        //Signals
        Container.DeclareSignal<GameStartSignal>();
        Container.DeclareSignal<BinsChangedSignal>();
        Container.DeclareSignal<PlayerHitSignal>();
        Container.DeclareSignal<TrashCollectedSignal>();
        Container.DeclareSignal<ScoreChangedSignal>();
        Container.DeclareSignal<TimeChangedSignal>();
        Container.DeclareSignal<TimeOverSignal>();
        Container.DeclareSignal<GameOverSignal>();

        //Pools
        Container.BindMemoryPool<Trash, Trash.Pool>()
            .WithInitialSize(8)
            .NonLazy();
        
        Container.BindMemoryPool<PointsEffectView, PointsEffectView.Pool>()
            .WithInitialSize(10)
            .FromComponentInNewPrefab(_gameGeneralSettings.PointEffectPrefab)
            .UnderTransform(new GameObject("PointsEffects").transform)
            .NonLazy();

        InstallDrivingSettings();
        InstallPlayers();
        InitExecutionOrder();
    }

    private void InstallPlayers()
    {
        for (var i = 0; i < _gameSettings.NumberOfPlayers; i++)
        {
            Container.BindInterfacesAndSelfTo<Player>()
                .FromSubContainerResolve()
                .ByInstaller<PlayerInstaller>()
                .AsTransient()
                .NonLazy();
        }
    }

    private void InstallDrivingSettings()
    {
        var settingsToInstall = _easyDrivingSettings;
        switch (_gameSettings.DifficultyLevel)
        {
            case DifficultyLevel.Easy:
                settingsToInstall = _easyDrivingSettings;
                break;
            case DifficultyLevel.Hard:
                settingsToInstall = _hardDrivingSettings;
                break;
            case DifficultyLevel.VeryHard:
                settingsToInstall = _veryHardDrivingSettings;
                break;
        }
        
        Container.BindInstance(settingsToInstall).AsSingle();
    }
    
    private void InitExecutionOrder()
    {
        Container.BindExecutionOrder<GameBorderSystem>(-10);
    }
}
