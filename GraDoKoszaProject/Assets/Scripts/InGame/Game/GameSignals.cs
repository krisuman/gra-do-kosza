﻿using Zenject;

public sealed class GameStartSignal : Signal<GameStartSignal> { }

public sealed class BinsChangedSignal : Signal<BinsChangedSignal> { }

public sealed class PlayerHitSignal : Signal<PlayerHitSignal, Player, GameElementVIew> { }

public sealed class TrashCollectedSignal : Signal<TrashCollectedSignal, Player, Trash, Result> { }

public sealed class ScoreChangedSignal : Signal<ScoreChangedSignal> { }

public sealed class TimeChangedSignal : Signal<TimeChangedSignal, int> { }

public sealed class TimeOverSignal : Signal<TimeOverSignal> { }

public sealed class GameOverSignal : Signal<GameOverSignal> { }