﻿using UnityEngine;
using Zenject;

public class TestPlayerController : ITickable
{
    [Inject]
    private GamePlayersHolderSystem _playersHolder;

    private void Start()
    {
        Application.targetFrameRate = 500;
    }

    public void Tick()
    {
        if (Input.GetKey(KeyCode.A))
        {
            TryTurnLeft(0);
        }
        if (Input.GetKey(KeyCode.D))
        {
            TryTurnRight(0);
        }
        
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            TryTurnLeft(1);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            TryTurnRight(1);
        }
        
        if (Input.GetKey(KeyCode.J))
        {
            TryTurnLeft(2);
        }
        if (Input.GetKey(KeyCode.J))
        {
            TryTurnRight(2);
        }
    }

    private void TryTurnLeft(int playerId)
    {
        Player player;
        _playersHolder.Players.TryGetValue(playerId, out player);
        if(null == player) {return;}
        player.TurnLeft();
    }

    private void TryTurnRight(int playerId)
    {
        Player player;
        _playersHolder.Players.TryGetValue(playerId, out player);
        if(null == player) {return;}
        player.TurnRight();
    }
}
