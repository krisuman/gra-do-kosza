﻿using System.Collections.Generic;

//TODO: remove maybe
public class GameViewsHolderSystem
{
    private static readonly List<GameElementVIew> AllGameViews = new List<GameElementVIew>();
    
    public List<GameElementVIew> AllVIews
    {
        get { return AllGameViews; }
    }

    public static void AddView(GameElementVIew view)
    {
        AllGameViews.Add(view);
    }

    public static void RemoveView(GameElementVIew view)
    {
        AllGameViews.Remove(view);
    }
}
    
