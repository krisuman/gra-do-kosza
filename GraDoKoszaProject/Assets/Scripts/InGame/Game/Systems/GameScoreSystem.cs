﻿using System.Linq;
using UnityEngine;
using Zenject;

public class GameScoreSystem : IInitializable
{
    private readonly GameGeneralSettings _gameGeneralSettings;
    private readonly GamePlayersHolderSystem _playersHolder;
    private readonly ScoreChangedSignal _scoreChangedSignal;
    private readonly PointsEffectView.Pool _pointsEffectsPool;
    
    public GameScoreSystem(
        GameGeneralSettings gameGeneralSettings,
        ScoreChangedSignal scoreChangedSignal,
        GamePlayersHolderSystem playersHolder,
        PointsEffectView.Pool pointsEffectsPool,
        TrashCollectedSignal trashCollectedSignal)
    {
        _gameGeneralSettings = gameGeneralSettings;
        _playersHolder = playersHolder;
        _pointsEffectsPool = pointsEffectsPool;
        _scoreChangedSignal = scoreChangedSignal;
        
        trashCollectedSignal.Listen(TrashCollected);
    }

    public void Initialize()
    {
        DefineResultPositions();
    }
    
    private void TrashCollected(Player player, Trash trash, Result result)
    {
        var pointsToAdd = result == Result.Succes
            ? _gameGeneralSettings.CorrectTrashCollectedPoints
            : _gameGeneralSettings.WrongTrashCollectedPoints;

        player.Score += pointsToAdd;
        Mathf.Clamp(player.Score, 0, int.MaxValue);

        _pointsEffectsPool.Spawn(player.Position, result == Result.Succes);
        
        DefineResultPositions();
        
        _scoreChangedSignal.Fire();
    }

    private void DefineResultPositions()
    {
        var playersInScoreOrder = _playersHolder.Players.Values.OrderByDescending(x => x.Score);
        var place = 1;
        foreach (var player in playersInScoreOrder)
        {
            player.ResultPosition = place++;
        }
    }
}