﻿using UnityEngine;
using Zenject;

public class GameBorderSystem : IInitializable
{
	//TODO: move to settings;
	private const float BorderOffset = 4;
	
	private readonly Camera _gameCamera;
	
	public GameBorders Borders { get; private set; }
	
	public GameBorderSystem(Camera gameCamera)
	{
		_gameCamera = gameCamera;
	}

	public void Initialize()
	{
		var xHalfExtends = _gameCamera.orthographicSize * ((float)Screen.width / Screen.height);
		var yHalfExtends = _gameCamera.orthographicSize;
		var leftX = _gameCamera.transform.position.x - xHalfExtends + BorderOffset;
		var rightX = _gameCamera.transform.position.x + xHalfExtends - BorderOffset;
		var topY = _gameCamera.transform.position.y + yHalfExtends - BorderOffset;
		var bottomY = _gameCamera.transform.position.y - yHalfExtends + BorderOffset;
		
		Borders = new GameBorders
		{
			LeftX = leftX,
			RightX = rightX,
			TopY = topY,
			BottomY = bottomY
		};

	}

    //TODO: change to class with private set
	public struct GameBorders
	{
		public float LeftX { get; set; }
		public float RightX { get; set; }
		public float TopY { get; set; }
		public float BottomY { get; set; }
	}
}
