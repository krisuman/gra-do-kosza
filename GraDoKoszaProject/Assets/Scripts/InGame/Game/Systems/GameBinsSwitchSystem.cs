﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameBinsSwitchSystem : IInitializable 
{
    private readonly BinsChangedSignal _binsChangedSignal;
    private readonly GameGeneralSettings _gameGeneralSettings;
    private readonly GamePlayersHolderSystem _playersHolder;

    private int _passedTime;
    private readonly List<BinType> _possibleBins = new List<BinType>();
    
    public GameBinsSwitchSystem(
        GamePlayersHolderSystem playersHolder,
        TimeChangedSignal timeChangedSignal,
        BinsChangedSignal trashsesChangedSignal,
        GameGeneralSettings gameGeneralSettings)
    {
        _binsChangedSignal = trashsesChangedSignal;
        _playersHolder = playersHolder;
        _gameGeneralSettings = gameGeneralSettings;
        
        timeChangedSignal.Listen(TimeChanged);
    }

    public void Initialize()
    {
        AssignRandomBins();
    }

    private void TimeChanged(int time)
    {
        _passedTime++;
        if (_passedTime >= _gameGeneralSettings.SwitchBinsTime)
        {
            _passedTime = 0;
            AssignRandomBins();
        }
    }
    
    private void AssignRandomBins()
    {
        _possibleBins.Clear();
        var bins = System.Enum.GetValues(typeof(BinType));
        foreach (var bin in bins)
        {
            _possibleBins.Add((BinType)bin);
        }
        
        foreach (var player in _playersHolder.Players)
        {
            var randomBin = _possibleBins[Random.Range(0, _possibleBins.Count)];
            _possibleBins.Remove(randomBin);
            player.Value.BinType = randomBin;
        }
        
        _binsChangedSignal.Fire();
    }
}