﻿public class GameTrashCollectSystem
{
    private readonly TrashCollectedSignal _trashCollectedSignal;
    
    public GameTrashCollectSystem(
        TrashCollectedSignal trashCollectedSignal,
        PlayerHitSignal playerHitSignal)
    {
        _trashCollectedSignal = trashCollectedSignal;
        playerHitSignal.Listen(PlayerHit);
    }

    private void PlayerHit(Player player, GameElementVIew gameElementVIew)
    {
        if (!(gameElementVIew is TrashView)) return;
        
        var trash = ((TrashView) gameElementVIew).ParentLink;
        var success = trash.BinType == player.BinType ? Result.Succes : Result.Failuer;
        _trashCollectedSignal.Fire(player, ((TrashView)gameElementVIew).ParentLink, success);
    }
}

public enum Result
{
    Succes,
    Failuer
}
