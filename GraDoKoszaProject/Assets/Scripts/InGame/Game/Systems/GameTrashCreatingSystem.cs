﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

public class GameTrashCreatingSystem : IInitializable
{
    private const float BorderOffset = 5f;
    
    private readonly Trash.Pool _trashPool;
    private readonly ProjectGeneralSettings _projectGeneralSettings;
    private readonly GameTrashesSettings _trashesSettings;
    private readonly GameBorderSystem _borders;
    private readonly List<string> _currentTrash = new List<string>();
    
    private Point[] _possibleSpawnPoints;
    private string _trashNameToRemove;
    
    public GameTrashCreatingSystem(
        Trash.Pool trashPool,
        ProjectGeneralSettings projectGeneralSettings,
        GameTrashesSettings trashesSettings,
        GameBorderSystem borders,
        TrashCollectedSignal trashCollectedSignal)
    {
        _trashPool = trashPool;
        _projectGeneralSettings = projectGeneralSettings;
        _trashesSettings = trashesSettings;
        _borders = borders;
        
        trashCollectedSignal.Listen(TryToCreateNewTrash);
    }

    public void Initialize()
    {
        DefinePossibleSpawnPoints();

        var iterations = _projectGeneralSettings.NumberOfPlayers < 3 ? 2 : 1;
        var values = System.Enum.GetValues(typeof(BinType));
        for (var i = 0; i < iterations; i++)
        {
            foreach (var value in values)
            {
                CreateNewTrash((BinType)value);
            }
        }
    }

    private void CreateNewTrash(BinType binType)
    {
        var trash = GetRandomTrashData(binType);
        _trashPool.Spawn(binType, trash, GetRandomPositionForTrash());
        _currentTrash.Remove(_trashNameToRemove);
        _currentTrash.Add(trash.Name);
    }

    private void TryToCreateNewTrash(Player player, Trash trash, Result result)
    {
        _trashNameToRemove = trash.Name;
        CreateNewTrash(trash.BinType);
    }

    private TrashData GetRandomTrashData(BinType binType)
    {
        var trashesGroup = _trashesSettings.Trashes[binType];
        var possibleTrash = trashesGroup.Where(x => !_currentTrash.Contains(x.Name)).ToList();
        return possibleTrash[Random.Range(0, possibleTrash.Count)];
    }

    private Vector2 GetRandomPositionForTrash()
    {
        ShuffleSpawnPoints();
        var positionToSpawn = Vector2.zero;
        for (var i = 0; i < _possibleSpawnPoints.Length; i++)
        {
            positionToSpawn.x = _possibleSpawnPoints[i].x;
            positionToSpawn.y = _possibleSpawnPoints[i].y;
            var overlap = Physics2D.OverlapCircle(positionToSpawn, 5);
            if (null == overlap)
            {
                break;
            }

        }

        return positionToSpawn;
    }

    private void DefinePossibleSpawnPoints()
    {
        var xSteps = Mathf.RoundToInt(_borders.Borders.RightX - BorderOffset) - Mathf.RoundToInt(_borders.Borders.LeftX + BorderOffset);
        var ySteps = Mathf.RoundToInt(_borders.Borders.TopY - BorderOffset) - Mathf.RoundToInt(_borders.Borders.BottomY + BorderOffset);
        var length = xSteps * ySteps;

        var xStartPoint = Mathf.RoundToInt(_borders.Borders.LeftX + BorderOffset);
        var yStartPoint = Mathf.RoundToInt(_borders.Borders.BottomY + BorderOffset);
        
        var index = 0;
        _possibleSpawnPoints = new Point[length];
        
        for (var i = 0; i < xSteps; i++)
        {
            for (var j = 0; j < ySteps; j++)
            {
                _possibleSpawnPoints[index] = new Point(xStartPoint + i, yStartPoint + j);
                index++;
            }
        }
    }
    
    private void ShuffleSpawnPoints()
    {
        var n = _possibleSpawnPoints.Length;
        for (var i = 0; i < n; i++) 
        {
            var r = i + Random.Range(0, n - i);
            
            var t = _possibleSpawnPoints[r];
            _possibleSpawnPoints[r] = _possibleSpawnPoints[i];
            _possibleSpawnPoints[i] = t;
        }
    }
    
    private struct Point
    {
        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int x;
        public int y;
    }
}
