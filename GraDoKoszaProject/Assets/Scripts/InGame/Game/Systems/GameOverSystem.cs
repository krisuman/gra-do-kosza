﻿using DG.Tweening;

public class GameOverSystem
{
    private readonly GameOverSignal _gameOverSignal;
    private readonly GamePauseSystem _gamePauseSystem;

    public GameOverSystem(
        GameOverSignal gameOverSignal,
        GamePauseSystem gamePauseSystem,
        TimeOverSignal timeOverSignal)
    {
        _gamePauseSystem = gamePauseSystem;
        _gameOverSignal = gameOverSignal;
        
        timeOverSignal.Listen(GameOver);
    }

    private void GameOver()
    {
        DOVirtual.DelayedCall(1f, () => _gamePauseSystem.PauseGame());
        _gameOverSignal.Fire();
    }
}
