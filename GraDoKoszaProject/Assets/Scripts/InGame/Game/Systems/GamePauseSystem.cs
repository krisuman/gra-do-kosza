﻿using UnityEngine;
using Zenject;

public class GamePauseSystem : IInitializable
{
    private bool _isPaused;
    
    public bool IsPaused
    {
        get { return _isPaused; }
    }

    public void Initialize()
    {
        UnpauseGame();
    }
    
    public void PauseGame()
    {
        _isPaused = true;
        Time.timeScale = 0f;
    }

    public void UnpauseGame()
    {
        _isPaused = false;
        Time.timeScale = 1f;
    }
}
