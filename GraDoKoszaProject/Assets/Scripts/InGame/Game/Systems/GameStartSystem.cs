﻿using System.Collections;
using UnityEngine;
using Zenject;

public class GameStartSystem : IInitializable
{
    private readonly AsyncProcessor _asyncProccessor;
    private readonly GameStartSignal _gameStartSignal;

    public GameStartSystem(
        AsyncProcessor asyncProccesor, 
        GameStartSignal gameStartSignal)
    {
        _gameStartSignal = gameStartSignal;
        _asyncProccessor = asyncProccesor;
    }

    public void Initialize()
    {
        _asyncProccessor.StartCoroutine(StartGame());
    }

    private IEnumerator StartGame()
    {
        //TODO: move time to some balancing settings
        yield return new WaitForSeconds(4.5f);
        _gameStartSignal.Fire();
    }
}
