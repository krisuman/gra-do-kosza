﻿public class GameTrashDestroySystem
{
    private readonly Trash.Pool _trashPool;

    public GameTrashDestroySystem(
        Trash.Pool trashPool,
        TrashCollectedSignal trashCollectedSignal)
    {
        _trashPool = trashPool;
        
        trashCollectedSignal.Listen(DestroyTrash);
    }

    private void DestroyTrash(Player player, Trash trash, Result result)
    {
        _trashPool.Despawn(trash);
    }
}
