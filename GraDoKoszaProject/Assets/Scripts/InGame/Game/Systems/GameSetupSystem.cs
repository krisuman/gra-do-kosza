﻿using UnityEngine;
using Zenject;

public class GameSetupSystem : IInitializable
{
    //TODO: move to some settings
    private const float PositionX = 30f;
    private const float PositionY = 12f;

    private readonly GamePlayersHolderSystem _playersHolder;
    
    public GameSetupSystem(GamePlayersHolderSystem playersHolder)
    {
        _playersHolder = playersHolder;
    }

    public void Initialize()
    {
        foreach(var player in _playersHolder.Players)
        {
            player.Value.Position = GetPositionBasedOnId(player.Key);
        }
    }

    private Vector2 GetPositionBasedOnId(int id)
    {
        var position = new Vector2();
        position.x = PositionX * (id == 0 || id == 3 ? -1 : 1);
        position.y = PositionY * (id == 0 || id == 1 ? -1 : 1);
        return position;
    }
}