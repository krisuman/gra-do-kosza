﻿using UnityEngine;
using Zenject;

public class GameTimerSystem : ITickable
{
    private readonly TimeOverSignal _timeOverSignal;
    private readonly TimeChangedSignal _timeChangedSignal;
    private readonly int _gameplayTime;
    
    private bool _timerStarted;
    private bool _timeOver;
    private float _timePased;
    private int _timeLeft;
    
    public GameTimerSystem(
        GameGeneralSettings gameGeneralSettings,
        TimeOverSignal timeOverSignal,
        TimeChangedSignal timeChangedSignal,
        GameStartSignal gameStartSignal)
    {
        _timeChangedSignal = timeChangedSignal;
        _timeOverSignal = timeOverSignal;
        _gameplayTime = gameGeneralSettings.GameplayTime;

        _timeLeft = _gameplayTime;
        
        gameStartSignal.Listen(() => _timerStarted = true);
    }
    
    public void Tick()
    {
        if(_timeOver) {return;}
        if(!_timerStarted) {return;}

        _timePased += Time.deltaTime;

        if (_timePased >= 1)
        {
            _timeLeft -= 1;
            _timeChangedSignal.Fire(_timeLeft);
            if (_timeLeft <= 0)
            {
                _timeOver = true;
                _timeOverSignal.Fire();
            }

            _timePased = 0;
        }
    }
}