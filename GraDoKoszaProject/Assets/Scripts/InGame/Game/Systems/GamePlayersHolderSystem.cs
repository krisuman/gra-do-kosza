﻿using System.Collections.Generic;

public class GamePlayersHolderSystem
{
    public Dictionary<int, Player> Players { get; private set; }

    public GamePlayersHolderSystem(List<Player> players, ProjectGeneralSettings gameSettings)
    {
        Players = new Dictionary<int, Player>();

        for(var i=0; i< players.Count; i++)
        {
            players[i].Id = i;
            Players.Add(i, players[i]);
        }
    }
}
