﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

[CreateAssetMenu(menuName = "GraDoKosza/GameTrashesSettings")]
public class GameTrashesSettings : ScriptableObjectInstaller
{
	[SerializeField]
	private Dictionary<BinType, TrashData[]> _trashesGroups;
	
    public Dictionary<BinType, TrashData[]> Trashes
    {
        get { return _trashesGroups; }
    }

    public override void InstallBindings()
    {
        Container.BindInstance(this).AsSingle();
    }
}


[System.Serializable]
public class TrashData
{
    public string Name;
    public string LocalizationId;
    public TrashView TrashPrefab;
    [PreviewField]
    public Sprite TrashIcon;
}
