﻿using UnityEngine;
using Zenject;

[CreateAssetMenu(menuName = "GraDoKosza/GameDrivingSettings")]
public class GameDrivingSettings : ScriptableObject
{
    [SerializeField]
    private float _drivingSpeed;
    [SerializeField]
    private float _turningSpeed;
    
    public float DrivingSpeed
    {
        get { return _drivingSpeed; }
    }

    public float TurningSpeed
    {
        get { return _turningSpeed; }
    }
}
