﻿using System.Collections.Generic;
using LocalizationEditor;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

[CreateAssetMenu(menuName = "GraDoKosza/GameBinsSettings")]
public class GameBinsSettings : ScriptableObjectInstaller
{
	[SerializeField]
	private Dictionary<BinType, Bin> _binsDictionary;
	
	public Dictionary<BinType, Bin> Bins
	{
		get { return _binsDictionary; }
	}

	public override void InstallBindings()
	{
		Container.BindInstance(this).AsSingle();
	}
}

[System.Serializable]
public class Bin
{
	public BinType BinType;
	public Color BinColor;
	public Color BinSecondaryColor;
	public RuntimeAnimatorController Animator;
    public Sprite BinSprite;
	[PreviewField]
	public Sprite BinLogoPolish;
	[PreviewField]
	public Sprite BinLogoEnglish;

	public Sprite BinLogo
	{
		get { return LEManager.CurrentLocSet == Constants.PolishLanguageSet ? BinLogoPolish : BinLogoEnglish; }
	}
}

public enum BinType
{
	green,
	blue,
	yellow,
	brown,
	gray
}
