﻿using Sirenix.Serialization;
using UnityEngine;
using Zenject;

[CreateAssetMenu(menuName = "GraDoKosza/GameGeneralSettings")]
public class GameGeneralSettings : ScriptableObjectInstaller
{
    [OdinSerialize]
    public int GameplayTime { get; private set; }
    [OdinSerialize]
    public int GameplayLowTime { get; private set; }
    [OdinSerialize]
    public int SwitchBinsTime { get; private set; }
    [OdinSerialize]
    public TrashView TrashView { get; private set; }
    [OdinSerialize]
    public int CorrectTrashCollectedPoints { get; private set; }
    [OdinSerialize]
    public int WrongTrashCollectedPoints { get; private set; }
    [OdinSerialize]
    public Sprite[] MedalSprites { get; private set; }
    [OdinSerialize]
    public PointsEffectView PointEffectPrefab { get; private set; }
    
    public override void InstallBindings()
    {
        Container.BindInstance(this).AsSingle();
    }
}