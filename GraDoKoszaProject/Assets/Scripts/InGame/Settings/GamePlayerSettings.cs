﻿using UnityEngine;
using Zenject;

[CreateAssetMenu(menuName = "GraDoKosza/GamePlayerSettings")]
public class GamePlayerSettings : ScriptableObjectInstaller
{
	[SerializeField]
	private PlayerView _playerView;
	[SerializeField]
	private PlayerDustEffect _dustEffect;
	[SerializeField]
	private PlayerDesriptionView _playerDesriptionView;
    
	public PlayerView PlayerView
	{
		get { return _playerView; }
	}

	public PlayerDustEffect DustEffect
	{
		get { return _dustEffect; }
	}

	public PlayerDesriptionView PlayerDesriptionView
	{
		get { return _playerDesriptionView; }
	}

	public override void InstallBindings()
	{
		Container.BindInstance(this).AsSingle();
	}
}