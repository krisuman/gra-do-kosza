﻿using UnityEngine;
using Zenject;

[RequireComponent(typeof(ParticleSystem))]
public class PlayerDustEffect : MonoBehaviour 
{
    public ParticleSystem ParticleSystem { get; private set; }

    private void Awake()
    {
        ParticleSystem = GetComponent<ParticleSystem>();
    }

    public class PlayerDustEffectPool : MemoryPool<Vector2, PlayerDustEffect>
    {
        protected override void Reinitialize(Vector2 positon, PlayerDustEffect item)
        {
            item.transform.position = positon;
            item.ParticleSystem.Play();
        }
    }
}
