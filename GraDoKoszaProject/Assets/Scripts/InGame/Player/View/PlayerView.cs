﻿using System;
using UnityEngine;

public class PlayerView : GameElementVIew
{
    [SerializeField]
    private Transform _leftWheel;
    [SerializeField]
    private Transform _rightWheel;
    [SerializeField]
    private SpriteRenderer _binRenderer;
    [SerializeField]
    private Animator _animator;

    public event Action<GameObject> OnCollision;

    public Vector2 Position
    {
        get { return transform.position; }
        set { transform.position = value; }
    }
    
    public Transform Transform
    { 
        get { return transform; }
    }

    public Animator Animator
    {
        get { return _animator; }
        set { _animator = value; }
    }

    public Transform LeftWheel
    {
        get { return _leftWheel; }
    }
    
    public Transform RightWheel
    {
        get { return _rightWheel; }
    }

    public SpriteRenderer BinRenderer
    {
        get { return _binRenderer; }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(OnCollision != null)
        {
            OnCollision(other.gameObject);
        }
    }
}
