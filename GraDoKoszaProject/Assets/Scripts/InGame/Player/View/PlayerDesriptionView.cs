﻿using DG.Tweening;
using LocalizationEditor;
using TMPro;
using UnityEngine;
using Zenject;

public class PlayerDesriptionView : MonoBehaviour
{
    private const float DefaultScale = 0.7f;
    private const float DefaultOffset = 2.8f;
    
    [Inject]
    private readonly GameBinsSettings _binsSettings;
    
    [SerializeField]
    private TextMeshPro _playerNameText;
    [SerializeField]
    private SpriteRenderer _binLogoRenderer;
    [SerializeField]
    private SpriteRenderer _pointerRenderer;

    private Player _player;
    private float _offsetMultiplier;
    private Vector3 _followPositon; 
    
    public void Show(Player player, bool upsideDown)
    {
        _player = player;
        gameObject.SetActive(true);
        transform.localScale = new Vector3(DefaultScale, DefaultScale, DefaultScale);
        transform.DOScale(0, 0.5f).From().SetEase(Ease.OutBack);
        
        transform.rotation = Quaternion.Euler(0,0,upsideDown ? 180 : 0);
        _offsetMultiplier = upsideDown ? -1 : 1;
        
        _playerNameText.text = string.Format("{0} {1}", LEManager.Get(LEKeys.player), _player.Id + 1);
        var binSettings = _binsSettings.Bins[player.BinType];
        _binLogoRenderer.sprite = binSettings.BinLogo;
        _pointerRenderer.color = binSettings.BinSecondaryColor;
    }

    public void Hide()
    {
        transform.DOScale(0, 0.5f).OnComplete(() =>
        {
            _player = null;
            gameObject.SetActive(false);
        });
    }

    private void Update()
    {
        if(null == _player) {return;}

        _followPositon.x = _player.Position.x;
        _followPositon.y = _player.Position.y + DefaultOffset * _offsetMultiplier;

        transform.position = _followPositon;
    }
}
