﻿using UnityEngine;
using Zenject;

public class PlayerInstaller : Installer
{
    [Inject]
    private readonly GamePlayerSettings _playerSettings;

    public override void InstallBindings()
    {
        Container.Bind<Player>().AsSingle();
        
        //View
        Container.Bind<PlayerView>().FromComponentInNewPrefab(_playerSettings.PlayerView).AsSingle().NonLazy();
        Container.Bind<PlayerDesriptionView>().FromComponentInNewPrefab(_playerSettings.PlayerDesriptionView).AsSingle().NonLazy();

        //Systems
        Container.BindInterfacesAndSelfTo<PlayerStartGameSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<PlayerOverGameSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<PlayerDrivingSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<PlayerDrivingEffectSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<PlayerTeleportSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<PlayerSetBinViewSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<PlayerCollisionSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<PlayerHitSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<PlayerHitImpactSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<PlayerAnimationsSystem>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<PlayerDescriptionSystem>().AsSingle().NonLazy();
        
        //Signals
        Container.DeclareSignal<ObstacleHitSignal>();
            
        //Pools
        Container.BindMemoryPool<PlayerDustEffect, PlayerDustEffect.PlayerDustEffectPool>()
            .WithInitialSize(15)
            .FromComponentInNewPrefab(_playerSettings.DustEffect)
            .UnderTransform(new GameObject("PlayerDustEffect").transform)
            .NonLazy();
    }
}
