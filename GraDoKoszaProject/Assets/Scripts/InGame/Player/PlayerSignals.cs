﻿using Zenject;

public sealed class ObstacleHitSignal : Signal<ObstacleHitSignal> {}
