﻿using UnityEngine;
using Zenject;

public class PlayerTeleportSystem : ITickable
{
    private const float TeleportOffset = 0.5f;
    
    private readonly PlayerView _playerView;
    private readonly GameBorderSystem _borderSystem;
    
    public PlayerTeleportSystem(PlayerView playerView, GameBorderSystem borderSystem)
    {
        _playerView = playerView;
        _borderSystem = borderSystem;
    }

    public void Tick()
    {
        if (_playerView.Transform.position.x < _borderSystem.Borders.LeftX)
        {
            _playerView.Position = new Vector2(-_playerView.Position.x - TeleportOffset, _playerView.Position.y);
        }
        
        if (_playerView.Transform.position.x > _borderSystem.Borders.RightX)
        {
            _playerView.Position = new Vector2(-_playerView.Position.x + TeleportOffset, _playerView.Position.y);
        }
        
        if (_playerView.Transform.position.y < _borderSystem.Borders.BottomY)
        {
            _playerView.Position = new Vector2(_playerView.Position.x, -_playerView.Position.y - TeleportOffset);
        }
        
        if (_playerView.Transform.position.y > _borderSystem.Borders.TopY)
        {
            _playerView.Position = new Vector2(_playerView.Position.x, -_playerView.Position.y + TeleportOffset);
        }
    }
}
