﻿public class PlayerSetBinViewSystem
{
    private readonly GameBinsSettings _binsSettings;
    private readonly Player _player;
    private readonly PlayerView _playerView;
    private readonly PlayerAnimationsSystem _playerAnimations;

    public PlayerSetBinViewSystem(
        GameBinsSettings trashsesSettings,
        Player player,
        PlayerView playerView,
        PlayerAnimationsSystem playerAnimations,
        BinsChangedSignal binChangedSignal)
    {
        _player = player;
        _binsSettings = trashsesSettings;
        _playerView = playerView;
        _playerAnimations = playerAnimations;

        binChangedSignal.Listen(ChangeTrashView);
    }

    private void ChangeTrashView()
    {
        var myBin = _binsSettings.Bins[_player.BinType];
        _playerAnimations.SetNewAnimator(myBin.Animator);
    }

}
