﻿using UnityEngine;
using DG.Tweening;

public class PlayerHitImpactSystem
{
    private readonly PlayerView _playerView;
    private readonly GameDrivingSettings _drivingSettings;
    
    public PlayerHitImpactSystem(
        PlayerView playerView,
        GameDrivingSettings gameDrivingSettings,
        ObstacleHitSignal obstacleHitSignal)
    {
        _playerView = playerView;
        _drivingSettings = gameDrivingSettings;
        
        obstacleHitSignal.Listen(ObstacleHit);
    }

    private void ObstacleHit()
    {
        var impactPosition = _playerView.Position + (Vector2)_playerView.Transform.TransformDirection(new Vector2(0, -(_drivingSettings.DrivingSpeed/4)));
        _playerView.Transform.DOMove(impactPosition, 0.5f);
    }
}
