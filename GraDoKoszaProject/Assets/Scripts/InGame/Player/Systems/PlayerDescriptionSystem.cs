﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PlayerDescriptionSystem
{
    private readonly Player _player;
    private readonly PlayerDesriptionView _desriptionView;
    
    public PlayerDescriptionSystem(
        Player player,
        PlayerDesriptionView desriptionView,
        BinsChangedSignal binsChangedSignal)
    {
        _player = player;
        _desriptionView = desriptionView;
        
        binsChangedSignal.Listen(ShowDescription);
        ShowDescription();
    }

    private void ShowDescription()
    {
        _desriptionView.Show(_player, _player.Id == 2 || _player.Id == 3);
        DOVirtual.DelayedCall(3f, () => _desriptionView.Hide()).SetUpdate(UpdateType.Normal, false);
    }
}
