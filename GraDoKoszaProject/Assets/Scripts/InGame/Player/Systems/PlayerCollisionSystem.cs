﻿using UnityEngine;

public class PlayerCollisionSystem
{
    private readonly Player _player;
    private readonly PlayerHitSignal _playerHitSignal;

    public PlayerCollisionSystem(
        Player player,
        PlayerHitSignal playerHitSignal,
        PlayerView playerView)
    {
        _player = player;
        _playerHitSignal = playerHitSignal;
        playerView.OnCollision += PlayerCollided;
    }

    private void PlayerCollided(GameObject gameObject)
    {
        var gameElement = gameObject.GetComponent<GameElementVIew>();
        if(null == gameElement) {return;}
        _playerHitSignal.Fire(_player, gameElement);
    }
}
