﻿using UnityEngine;
using Zenject;

public class PlayerDrivingSystem : ITickable
{
    private readonly PlayerView _playerView;
    private readonly GameDrivingSettings _drivingSettings;
    private readonly PlayerAnimationsSystem _playerAnimations;
    
    private readonly Vector3 _rotatingVector = new Vector3(0,0,1);
    private readonly Vector3 _driveVector = new Vector3(0,1,0);
    
    private bool _drivingEnabled;
    
    public bool IsDriving
    {
        get { return _drivingEnabled; }
    }
    
    public PlayerDrivingSystem(
        PlayerView playerView,
        PlayerAnimationsSystem playerAnimations,
        GameDrivingSettings drivingSettings)
    {
        _playerAnimations = playerAnimations;
        _playerView = playerView;
        _drivingSettings = drivingSettings;
    }

    public void StartDriving()
    {
        _drivingEnabled = true;
        _playerAnimations.PlayDrivingAnimation();
    }

    public void StopDriving()
    {
        _playerAnimations.PlayIdleAnimation();
        _drivingEnabled = false;
    }

    public void TurnLeft()
    {
        Turn(TurnDirection.Left);
    }

    public void TurnRight()
    {
        Turn(TurnDirection.Right);
    }
    
    public void Tick()
    {    
        Drive();
    }

    private void Drive()
    {
        if(!_drivingEnabled) {return;}
        _playerView.Transform.Translate(_driveVector * _drivingSettings.DrivingSpeed * Time.deltaTime);
    }

    private void Turn(TurnDirection direction)
    {
        _playerView.Transform.Rotate(_rotatingVector * (int)direction * _drivingSettings.TurningSpeed * Time.deltaTime);
    }
    
    private enum TurnDirection
    {
        Left = 1,
        Right = -1
    }
}
