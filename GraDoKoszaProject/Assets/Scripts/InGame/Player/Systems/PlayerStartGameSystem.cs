﻿public class PlayerStartGameSystem
{
    private readonly PlayerDrivingSystem _drivingSystem;

    public PlayerStartGameSystem(GameStartSignal gameStartSignal, PlayerDrivingSystem drivingSystem)
    {
        _drivingSystem = drivingSystem;
        gameStartSignal.Listen(StartGame);
    }

    private void StartGame()
    {
        _drivingSystem.StartDriving();
    }
}
