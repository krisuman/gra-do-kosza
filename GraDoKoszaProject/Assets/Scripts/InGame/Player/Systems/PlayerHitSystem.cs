﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHitSystem
{
    private readonly AsyncProcessor _asyncProcessor;
    private readonly PlayerDrivingSystem _playerDrivingSystem;
    private readonly Player _player;
    private readonly ObstacleHitSignal _obstacleHitSignal;
    
    public PlayerHitSystem(
        AsyncProcessor asyncProcessor,
        PlayerHitSignal playerHitSignal,
        PlayerDrivingSystem playerDrivingSystem,
        ObstacleHitSignal obstacleHitSignal,
        Player player)
    {
        _asyncProcessor = asyncProcessor;
        _playerDrivingSystem = playerDrivingSystem;
        _player = player;
        _obstacleHitSignal = obstacleHitSignal;
        
        playerHitSignal.Listen(PlayerHitted);
    }

    private void PlayerHitted(Player player, GameElementVIew gameElementVIew)
    {
        if(player != _player) {return;}
        if(_player.IsCoolDown) {return;}

        if (gameElementVIew is ObstacleView)
        {
            _playerDrivingSystem.StopDriving();
            _player.IsCoolDown = true;
            _obstacleHitSignal.Fire();
            _asyncProcessor.StartCoroutine(AfterHitCouldown());
        }
    }

    private IEnumerator AfterHitCouldown()
    {
        //TODO: move this to some settings
        yield return new WaitForSeconds(3);
        _playerDrivingSystem.StartDriving();
        _player.IsCoolDown = false;
    }
}
