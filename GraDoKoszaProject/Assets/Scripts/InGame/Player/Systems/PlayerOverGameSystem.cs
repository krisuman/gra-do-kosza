﻿public class PlayerOverGameSystem
{
	private readonly PlayerDrivingSystem _drivingSystem;

	public PlayerOverGameSystem(GameOverSignal gameOverSignal, PlayerDrivingSystem drivingSystem)
	{
		_drivingSystem = drivingSystem;
		gameOverSignal.Listen(OverGame);
	}

	private void OverGame()
	{
		_drivingSystem.StopDriving();
	}
}