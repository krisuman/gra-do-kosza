﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PlayerDrivingEffectSystem : ITickable
{
    private readonly PlayerView _playerView;
    private readonly PlayerDrivingSystem _drivingSystem;
    private readonly PlayerDustEffect.PlayerDustEffectPool _dustEffectPool;

    private float _timeSinceSpawn;
    private float _timeSinceDespawn;
    private Queue<PlayerDustEffect> _effects = new Queue<PlayerDustEffect>();
    
    public PlayerDrivingEffectSystem(
        PlayerView playerView,
        PlayerDrivingSystem drivingSystem,
        PlayerDustEffect.PlayerDustEffectPool dustEffectPool)
    {
        _playerView = playerView;
        _drivingSystem = drivingSystem;
        _dustEffectPool = dustEffectPool;
    }
    
    public void Tick()
    {
        if (!_drivingSystem.IsDriving)
        {
            return;
        }

        _timeSinceSpawn += Time.deltaTime;
        _timeSinceDespawn += Time.deltaTime;
        
        if (_timeSinceSpawn < 0.1f)
        {
            return;
        }

        _timeSinceSpawn = 0;
        _effects.Enqueue(_dustEffectPool.Spawn(_playerView.LeftWheel.position));
        _effects.Enqueue(_dustEffectPool.Spawn(_playerView.RightWheel.position));

        if (_timeSinceDespawn < 2f || _effects.Count < 2)
        {
            return;
        }

        //_timeSinceDespawn = 0;
        _dustEffectPool.Despawn(_effects.Dequeue());
        _dustEffectPool.Despawn(_effects.Dequeue());
    }
}
