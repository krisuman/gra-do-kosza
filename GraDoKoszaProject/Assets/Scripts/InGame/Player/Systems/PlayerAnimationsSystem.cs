﻿using UnityEngine;

public class PlayerAnimationsSystem
{
	private const string StateDrivingName = "driving";
	private const string StateStartingName = "starting";
	
	private readonly PlayerView _playerView;

	private readonly int _triggerIdle = Animator.StringToHash("idle");
	private readonly int _triggerStartDriving = Animator.StringToHash("start_driving");
	private readonly int _triggerDriving = Animator.StringToHash("driving");

//	private readonly int _stateIdleName = Animator.StringToHash("idle");
//	private readonly int _stateStartingName = Animator.StringToHash("starting");
//	private readonly int _stateDrivingName = Animator.StringToHash("driving");
	
	public PlayerAnimationsSystem(
		PlayerView playerView)
	{
		_playerView = playerView;
	}

	public void PlayIdleAnimation()
	{
		if(null == _playerView.Animator) {return;}
		_playerView.Animator.SetTrigger(_triggerIdle);
	}
	
	public void PlayDrivingAnimation(bool withStarting = true)
	{
		if(null == _playerView.Animator) {return;}
		_playerView.Animator.SetTrigger(withStarting ? _triggerStartDriving : _triggerDriving);
	}

	public void SetNewAnimator(RuntimeAnimatorController animatorController)
	{	
		var currentStateInfo = _playerView.Animator.GetCurrentAnimatorStateInfo(0);
		var playDriving = currentStateInfo.IsName(StateDrivingName) || currentStateInfo.IsName(StateStartingName);
		_playerView.Animator.runtimeAnimatorController = animatorController;
		if (playDriving)
		{
			PlayDrivingAnimation(false);
		}
	}
}
