﻿using UnityEngine;
using Zenject;

public class Player : Kernel
{
    private readonly PlayerView _playerView;
    private readonly PlayerDrivingSystem _playerDrivingSystem;
    
    public Player(
        PlayerView playerView,
        PlayerDrivingSystem drivingSystem)
    {
        _playerView = playerView;
        _playerDrivingSystem = drivingSystem;
    }

    public int Id { get; set; }

    public int Score { get; set; }
    
    public int ResultPosition { get; set; }
    
    public BinType BinType { get; set; }

    public bool IsCoolDown { get; set; }
    
    public Vector2 Position
    {
        get { return _playerView.Position; }
        set { _playerView.Position = value; }
    }

    public void TurnLeft()
    {
        _playerDrivingSystem.TurnLeft();
    }

    public void TurnRight()
    {
        _playerDrivingSystem.TurnRight();
    }
}
