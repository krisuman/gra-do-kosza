﻿using UnityEngine;
using Zenject;

public abstract class GameElementVIew : MonoBehaviour
{
    private void OnEnable()
    {
        GameViewsHolderSystem.AddView(this);
    }
    
    private void OnDisable()
    {
        GameViewsHolderSystem.RemoveView(this);
    }
}
