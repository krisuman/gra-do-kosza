﻿using UnityEngine;

namespace Playsfot.Zombicide.Tools
{
    [ExecuteInEditMode]
    [RequireComponent (typeof(Renderer))]
    public class RendererLayerSetter : MonoBehaviour
    {
        [SerializeField]
        private string sotringLayer;
        [SerializeField]
        private int orderInLayer;

        private void Awake()
        {
            var renderer = GetComponent<Renderer>();
            renderer.sortingLayerName = sotringLayer;
            renderer.sortingOrder = orderInLayer;
        }

#if UNITY_EDITOR
        private void Update()
        {
            if(!Application.isPlaying)
            {
                var renderer = GetComponent<Renderer>();
                renderer.sortingLayerName = sotringLayer;
                renderer.sortingOrder = orderInLayer;
            }
        }
#endif
    }
}

