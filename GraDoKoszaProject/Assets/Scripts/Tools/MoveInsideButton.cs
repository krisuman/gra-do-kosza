﻿using UnityEngine;
using UnityEngine.UI;

public class MoveInsideButton : Button
{
	[SerializeField]
	private RectTransform _insideRect;
	[SerializeField]
	private Vector2 _normalPosition;
	[SerializeField]
	private Vector2 _pressedPosition;
	
	protected override void DoStateTransition(SelectionState state, bool instant)
	{
		base.DoStateTransition(state, instant);

		if(null == _insideRect) {return;}
		switch (state)
		{
			case SelectionState.Pressed:
				_insideRect.anchoredPosition = _pressedPosition;
				break;
			default:
				_insideRect.anchoredPosition = _normalPosition;
				break;
			
		}
	}
}
