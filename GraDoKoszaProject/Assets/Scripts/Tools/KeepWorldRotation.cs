﻿using UnityEngine;

public class KeepWorldRotation : MonoBehaviour
{
	[SerializeField]
	private Vector3 rotationToKeep;

	public Vector3 RotationToKeep
	{
		get { return rotationToKeep; }
		set { rotationToKeep = value; }
	}

	private Quaternion lastRotation;
	
	private void Update()
	{
		if(lastRotation == transform.rotation) {return;}

		transform.rotation = Quaternion.Euler(rotationToKeep);
		lastRotation = transform.rotation;
	}
}
