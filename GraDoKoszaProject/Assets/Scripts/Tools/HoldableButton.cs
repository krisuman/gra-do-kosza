﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class HoldableButton : Button
{
    [FormerlySerializedAs("onHold")]
    [SerializeField]
    private ButtonClickedEvent m_OnHold = new ButtonClickedEvent();

    private bool _isHolding;
    
    public ButtonClickedEvent onHold
    {
        get
        {
            return this.m_OnHold;
        }
        set
        {
            this.m_OnHold = value;
        }
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        _isHolding = true;
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);
        _isHolding = false;
    }

    private void Update()
    {
        if (_isHolding)
        {
            m_OnHold.Invoke();
        }
    }
    
    [Serializable]
    public class ButtonHoldEvent : UnityEvent
    {
    }
}
