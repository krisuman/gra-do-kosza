﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/2D/SimpleWater"
{
    Properties {
        _MainTex ("Main texture", 2D) = "white" {}
        _MaskTex ("Mask texture", 2D) = "white" {}
        _MainTint ("Tint", Color) = (1,1,1,1)
        _ScrollXSpeed("X scroll speed", Range(0,10)) = 2
        _ScrollYSpeed("Y scroll speed", Range(0,10)) = 2
    }
 
    SubShader {
 
        Tags { "Queue"="Transparent" }
        Blend SrcAlpha OneMinusSrcAlpha
 
        Pass {
           
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
 
 
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };
 
            struct v2f {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
            };
           
           fixed4 _MainTint;
           fixed _ScrollXSpeed;
           fixed _ScrollYSpeed;
           sampler2D _MainTex;
           sampler2D _MaskTex;
           
           fixed4 _MainTex_ST;
           
            v2f vert(appdata v) {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                //o.pos = UnityObjectToClipPos(v.vertex);
                //o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                return o;
            }

 
            fixed4 frag(v2f i) : SV_Target {
                fixed xScrollValue = _ScrollXSpeed * _Time;
                fixed yScrollValue = _ScrollYSpeed * _Time;
            
                float4 maskColor = tex2D(_MaskTex, i.uv);
            
                i.uv += fixed2(xScrollValue, yScrollValue);

               float4 color = tex2D(_MainTex, i.uv);
               color *= _MainTint;
               color.a = maskColor.a;
               return color;                
            }
            ENDCG
        }
    }
}